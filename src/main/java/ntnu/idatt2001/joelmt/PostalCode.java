package ntnu.idatt2001.joelmt;

import java.util.Objects;

/**
 * Class representing a postal code. The object
 * contains information regarding the postal code and city, and
 * the municipality it is located in
 *
 */
public class PostalCode {

    private String postalCode;
    private String city;
    private String municipality;

    /**
     * Constructor that creates the postal code object with all the parameter
     *
     * @param postalCode the city's postal code
     * @param city the city name
     * @param municipality the name of the municipality the city is located in
     */
    public PostalCode(String postalCode, String city, String municipality) {
        try {
            Integer.parseInt(postalCode);
        } catch (Exception e) {
            throw new IllegalArgumentException("The postal code must only consist of numbers.");
        }
        if (postalCode.length() != 4) {
            throw new IllegalArgumentException("The postal code must be 4 digits long.");
        }
        this.postalCode = postalCode;
        this.city = city;
        this.municipality = municipality;
    }

    /**
     * Getter for the postal code
     *
     * @return postal code number
     *
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Getter for the name of the city
     *
     * @return city name
     *
     */
    public String getCity() {
        return city;
    }

    /**
     * Getter for the name of the municipality
     * the city is located in
     *
     * @return name of the municipality
     *
     */
    public String getMunicipality() {
        return municipality;
    }

    /**
     * Method for comparing two postal codes to see if they are equal.
     * Postal codes are considered equal if every property is equal
     *
     * @param o object to compare to
     * @return true is object are equal, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostalCode that = (PostalCode) o;
        return Objects.equals(postalCode, that.postalCode) &&
                Objects.equals(city, that.city) &&
                Objects.equals(municipality, that.municipality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postalCode, city, municipality);
    }

    /**
     * Method for getting all the information of a postal code
     * as a String
     *
     * @return String containing all the information of the postal code
     */
    public String getAsString() {
        return postalCode + "\t" + city + "\t" + municipality;
    }
}
