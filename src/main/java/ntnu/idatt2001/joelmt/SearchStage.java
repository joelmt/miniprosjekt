package ntnu.idatt2001.joelmt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for creating the stage
 * for searching the register
 *
 */
public class SearchStage extends Stage {
    private PostalCodeRegister data;
    private TableView tableView = PostalCodeApplication.createTableView();
    private TextField searchField = new TextField();
    private ChoiceBox<String> searchTypeBox = new ChoiceBox<>();

    /**
     * Constructor responsible for creating the stage
     * and setting the data
     *
     * @param data The data to be displayed in the stages TableView
     *
     */
    public SearchStage(PostalCodeRegister data) {
        super();
        this.data = data;
        createStage();
    }

    /**
     * Method for setting up the SearchStage with all the
     * graphical elements and button functionality
     *
     */
    private void createStage() {
        searchField.setPromptText("Search");
        searchField.setPrefWidth(70);

        //Creating and setting up the GridPane
        GridPane gridPane = new GridPane();
        gridPane.setHgap(13);
        gridPane.setVgap(6);
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        //Creating and setting up the text fields label, and adding it to the GridPane
        Label searchLabel = new Label();
        searchLabel.setText("Search");
        gridPane.add(searchLabel,1,1);
        gridPane.add(searchField, 2, 1, 19, 1);

        //Creating and setting up the search button, and adding it to the GridPane
        Button searchButton = new Button();
        searchButton.setText("Search");
        gridPane.add(searchButton, 7, 2, 4, 1);
        GridPane.setHalignment(searchButton, HPos.LEFT);

        //Setting up the choice box, and adding it to the GridPane
        searchTypeBox.getItems().addAll("Search using postal code", "Search using city name");
        searchTypeBox.setValue("Search using postal code");
        gridPane.add(searchTypeBox, 1, 2, 6, 1);

        gridPane.add(tableView, 1, 3, 30, 1);

        //Creating a scene with the GridPane, and setting up the stage
        Scene taskScene = new Scene(gridPane, 620, 300);
        super.setTitle("Search the register");
        super.setScene(taskScene);
        super.initModality(Modality.APPLICATION_MODAL);

        searchButton.setOnAction(actionEvent -> {
            //Checking if anything is wrong with the input, and showing an alert if something is wrong
            if (validateInput().isBlank()) {
                //Setting different actionEvent depending on what is chosen in the choice box
                if (searchTypeBox.getValue().equalsIgnoreCase("Search using postal code")) {
                    updateTableView(data.searchUsingNumber(searchField.getText().trim()));
                } else if (searchTypeBox.getValue().equalsIgnoreCase("Search using city name")) {
                    updateTableView(data.searchUsingCity(searchField.getText().trim()));
                }
            } else {
                createWarningAlert(validateInput());
            }
        });
    }

    /**
     * Method for validating the user input from the
     * text field. It creates a String containing all the
     * feedback for the user.
     *
     * @return String containing feedback for the user regarding the input
     *
     */
    private String validateInput(){
        String explanation = "";

        if (searchField.getText().isBlank()) {
            explanation += "Search field cannot be empty.\n";
        }

        if (searchTypeBox.getValue().equalsIgnoreCase("Search using postal code")) {
            try {
                Integer.parseInt(searchField.getText().trim());
            } catch (Exception e) {
                explanation += "Postal code must be numbers";
            }
        }
        return explanation;
    }

    /**
     * Method for creating and showing an alert
     * for when the user input is invalid.
     *
     * @param explanation String containing feedback for the user regarding the input
     *
     */
    private void createWarningAlert(String explanation){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Invalid input");
        alert.setHeaderText("Invalid input");
        alert.setContentText(explanation);
        alert.showAndWait();
    }

    /**
     * Method for updating the information displayed in the TableView
     * to match the contents of the ArrayList from the search
     *
     * @param list List containing the data to be displayed in the TableView
     *
     */
    private void updateTableView(List<PostalCode> list) {
        List<PostalCode> dataList = list;
        ObservableList<PostalCode> dataObservableList = FXCollections.observableList(dataList);
        tableView.setItems(dataObservableList);
        tableView.refresh();
    }
}
