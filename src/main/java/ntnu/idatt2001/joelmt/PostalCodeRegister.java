package ntnu.idatt2001.joelmt;

import java.util.ArrayList;

/**
 * Class representing a register of postal codes
 *
 */
public class PostalCodeRegister {
    private ArrayList<PostalCode> postalCodeRegister;

    /**
     * Constructor responsible for initiating the register ArrayList
     *
     */
    public PostalCodeRegister() {
        postalCodeRegister = new ArrayList<>();
    }

    /**
     * Getter for the register ArrayList
     *
     * @return the register
     */
    public ArrayList<PostalCode> getPostalCodeRegister() {
        return postalCodeRegister;
    }

    /**
     * Method for adding a postal code to the register
     *
     * @param postalCode object to compare to
     * @return true is the postal code was added successfully, false otherwise
     */
    public boolean addPostalCode(PostalCode postalCode) {
        if(!postalCodeRegister.contains(postalCode)){
            postalCodeRegister.add(postalCode);
            return true;
        }
        return false;
    }

    /**
     * Method for searching through the register for a postal code
     * with or containing a specific postal code number using a search string
     *
     * @param searchString string containing the numbers to be searched for
     * @return ArrayList containing the results of the search
     */
    public ArrayList<PostalCode> searchUsingNumber(String searchString) {
        ArrayList<PostalCode> results = new ArrayList<>();

        for (PostalCode p : postalCodeRegister) {
            if (p.getPostalCode().equalsIgnoreCase(searchString)) {
                results.add(p);
            } else if (p.getPostalCode().contains(searchString)) {
                results.add(p);
            }
        }
        return results;
    }

    /**
     * Method for searching through the register for a postal code
     * with or containing a specific city name using a search string
     *
     * @param searchString string containing the name/substring to be searched for
     * @return ArrayList containing the results of the search
     */
    public ArrayList<PostalCode> searchUsingCity(String searchString) {
        ArrayList<PostalCode> results = new ArrayList<>();

        for (PostalCode p : postalCodeRegister) {
            if (p.getCity().equalsIgnoreCase(searchString)) {
                results.add(p);
            } else if (p.getCity().toUpperCase().contains(searchString.toUpperCase())) {
                results.add(p);
            }
        }
        return results;
    }

    /**
     * Method for getting the entire register as a String
     *
     * @return String containing the entire register
     */
    @Override
    public String toString() {
        return "PostalCodeRegister{" +
                postalCodeRegister +
                '}';
    }
}
