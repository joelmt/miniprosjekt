package ntnu.idatt2001.joelmt;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Class responsible for creating and viewing the PostalCodeRegister application
 *
 */
public class PostalCodeApplication extends Application {
    TableView<PostalCode> tableView;
    PostalCodeRegister data = new PostalCodeRegister();

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Method that is run when the application is started.
     * It is responsible for setting up the primaryStage, adding
     * the necessary elements to the pane and adding everything
     * to the primaryStage
     *
     * @param primaryStage the stage where the main application is shown
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Postal Code Register");
        tableView = createTableView();

        //Creating and setting up the GridPane
        GridPane gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);

        //Adding the MenuBar and tableView to the GridPane
        gridPane.add(createMenu(), 1, 1);
        gridPane.add(tableView, 1, 2);

        Scene scene = new Scene(gridPane, 620, 300 );
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method for creating a TableView containing all the columns
     * needed to display all the information of a postal code object
     *
     * @return TableView set up for displaying PostalCode objects
     */
    public static TableView createTableView() {
        //Creating and setting up the TableView
        TableView newTableView = new TableView();
        newTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        newTableView.setPrefSize(600, 200);

        //Creating and setting up every column with the correct names and values
        TableColumn<PostalCode, String> column1 = new TableColumn<>("Postal Code");
        column1.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        TableColumn<PostalCode, String> column2 = new TableColumn<>("City");
        column2.setCellValueFactory(new PropertyValueFactory<>("city"));
        TableColumn<PostalCode, String>  column3 = new TableColumn<>("Municipality");
        column3.setCellValueFactory(new PropertyValueFactory<>("municipality"));

        newTableView.getColumns().addAll(column1, column2, column3);

        return newTableView;
    }

    /**
     * Method for creating the mMnuBar shown at the top of the application
     *
     * @return a MenuBar with all the Menus and MenuItems needed for the application
     */
    private MenuBar createMenu() {
        MenuBar menuBar = new MenuBar();
        Menu menuFile = new Menu("File");
        Menu menuSearch = new Menu("Search");

        //Creating the menuItem for importing a file and setting its actionEvent
        MenuItem importFile = new MenuItem("Import data from file");
        importFile.setOnAction(actionEvent -> importData());

        //Creating the menuItem for searching and setting its actionEvent
        MenuItem search = new MenuItem("Search using postal code/city name");
        search.setOnAction(actionEvent -> {
            SearchStage searchStage = new SearchStage(data);
            searchStage.showAndWait();
        });

        menuFile.getItems().addAll(importFile);
        menuSearch.getItems().addAll(search);

        menuBar.getMenus().addAll(menuFile, menuSearch);

        return menuBar;
    }

    /**
     * Method for importing data for a .txt file
     *
     */
    private void importData() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());

        String fileExtension;
        String fileName;

        if (file != null) {
            fileName = file.toString();

            //Retrieves the extension of the selected file
            fileExtension = fileName.substring(fileName.lastIndexOf("."));

            //Checking if the file extension is incorrect, and showing an alert if it is
            if (!fileExtension.equals(".txt")) {
                fileExtensionAlert();
                importData();
            }

            if (fileExtension.equals(".txt")) {
                BufferedReader fileReader = null;
                try {
                    PostalCodeRegister postalCodeRegister = new PostalCodeRegister();
                    String line;

                    fileReader = new BufferedReader(new FileReader(fileName));

                    //Reading every line in the file, line by line
                    while ((line = fileReader.readLine()) != null) {

                        //Created an array by splitting the line on the tabulator
                        String[] fields = line.split("\t");

                        if (fields.length > 0) {
                            //Retrieving the correct fields, creating a PostalCode object and adding it to the register
                            PostalCode postalCode = new PostalCode(fields[0], fields[1], fields[3]);
                            postalCodeRegister.addPostalCode(postalCode);
                        }
                    }
                    //Setting the applications data to the data from the file
                    data = postalCodeRegister;
                } catch(Exception e) {
                    fileReadingAlert();
                    System.out.println("Error in FileReader");
                    System.out.println(e);
                } finally {
                    try {
                        fileReader.close();
                    } catch (IOException e) {
                        System.out.println("Error while closing fileReader.");
                    }
                }
                update();
            }
        }
    }

    /**
     * Method for updating the information displayed in the TableView
     * to match the contents of the PostalCodeRegister
     *
     */
    private void update() {
        List<PostalCode> dataList = data.getPostalCodeRegister();
        ObservableList<PostalCode> dataObservableList = FXCollections.observableList(dataList);
        tableView.setItems(dataObservableList);
        tableView.refresh();
    }

    /**
     * Method for creating and displaying an alert
     * for when the selected file for importing has
     * an invalid file extension
     *
     */
    private void fileExtensionAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("File Extension");
        alert.setHeaderText("Invalid file extension");
        alert.setContentText("The chosen file extension is invalid.\nPlease choose a file with the .txt file extension.");
        alert.showAndWait();
    }

    /**
     * Method for creating and displaying an alert
     * for when an error occurs when importing
     * and reading a file
     *
     */
    private void fileReadingAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error");
        alert.setHeaderText("Error while reading file");
        alert.setContentText("An error occurred while reading the selected file.\nPlease make sure that the contents of the file are valid.");
        alert.showAndWait();
    }
}
