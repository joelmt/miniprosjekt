package ntnu.idatt2001.joelmt;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for testing methods
 * in the PostalCodeRegister class
 *
 */
class PostalCodeRegisterTest {
    PostalCodeRegister register = new PostalCodeRegister();
    PostalCode postalCode = new PostalCode("1642", "Saltnes",  "Råde");

    /**
     * Method that tests if the method
     * for adding a postal code to the register
     * works as intended when the postal code does
     * not already exists in the register
     *
     */
    @Test
    void addNewPostalCode() {
        assertTrue(register.addPostalCode(postalCode));
        assertTrue(register.getPostalCodeRegister().contains(postalCode));
    }

    /**
     * Method that tests if the method
     * for adding a postal code to the register
     * works as intended when the postal code
     * already exists in the register
     *
     */
    @Test
    void addAlreadyExistingPostalCode() {
        register.addPostalCode(postalCode);
        assertFalse(register.addPostalCode(postalCode));
    }

    /**
     * Method that tests if the method
     * for searching for a postal code using the code
     * works as intended when the postal code
     * exists in the register
     *
     */
    @Test
    void searchUsingNumberThroughNonEmptyArray() {
        assertTrue(register.addPostalCode(postalCode));
        assertTrue(register.searchUsingNumber("1642").contains(postalCode));
    }

    /**
     * Method that tests if the method
     * for searching for a postal code using the code
     * works as intended when the postal code
     * does not exist in the register
     *
     */
    @Test
    void searchUsingNumberThroughEmptyArray() {
        assertTrue(register.searchUsingNumber("1642").isEmpty());
    }

    /**
     * Method that tests if the method
     * for searching for a postal code using the city
     * works as intended when the postal code
     * exists in the register
     *
     */
    @Test
    void searchUsingCityThroughNonEmptyArray() {
        assertTrue(register.addPostalCode(postalCode));
        assertTrue(register.searchUsingCity("Saltnes").contains(postalCode));
    }

    /**
     * Method that tests if the method
     * for searching for a postal code using the city
     * works as intended when the postal code
     * does not exist in the register
     *
     */
    @Test
    void searchUsingCityThroughEmptyArray() {
        assertTrue(register.searchUsingCity("Saltnes").isEmpty());
    }
}